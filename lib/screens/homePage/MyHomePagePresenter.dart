import 'package:fmvp/screens/homePage/MyHomePageContract.dart';

class MyHomePagePresenter {
  MyHomePageContract _view;

  MyHomePagePresenter(this._view);

  iWasPressed() {
    _view.doSomething();
  }
}