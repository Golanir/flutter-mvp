import 'package:flutter/material.dart';
import 'package:fmvp/screens/homePage/MyHomePageContract.dart';
import 'package:fmvp/screens/homePage/MyHomePagePresenter.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> implements MyHomePageContract{

  MyHomePagePresenter _presenter;


  _MyHomePageState() {
    _presenter = new MyHomePagePresenter(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0.0, 100.0, 0.0, 0.0),
              child: Text(
                'hello',
                style: TextStyle(color: Colors.green, fontSize: 35.0),
              ),
            ),
            RaisedButton(
              onPressed: () => _presenter.iWasPressed(),
              child: Text('boom'),
            )
          ],
        ),
      ),
    );
  }

  @override
  void doSomething() {
    debugPrint('asdasdasdas');
  }
}
