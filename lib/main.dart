import 'package:flutter/material.dart';
import 'package:fmvp/screens/homePage/MyHomePageView.dart';

void main() {
  runApp(MaterialApp(
    title: 'Flutter MVP',
    debugShowCheckedModeBanner: false,
    home: MyHomePage(),
  ));
}